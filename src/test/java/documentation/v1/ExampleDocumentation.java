package documentation.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.ub.core.base.utils.RequestUtils.AUTH_TOKEN;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
public class ExampleDocumentation extends BaseDocumentation {
    @Override
    public void onInit() {
        super.onInit();
        super.init();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        super.destroy();
    }

    @Test
    public void exampleDocumentation() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(post("/api/example")
                        .accept(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content(mapper.writeValueAsString(new Object()))
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
//                .header(AUTH_TOKEN, kokorinAccessToken)
        )
                .andExpect(status().isOk());
    }

    private FieldDescriptor[] exampleResponse() {
        return new FieldDescriptor[]{
                fieldWithPath("result").description("Результат"),
                fieldWithPath("result.count").description("Количество"),
                fieldWithPath("result.items").description("Массив"),
                fieldWithPath("result.items[].example").description("Пример"),
        };
    }

    private FieldDescriptor[] exampleRequest() {
        return new FieldDescriptor[]{
                fieldWithPath("text").description("Текст коментария (обязательно)"),
                fieldWithPath("parentId").description("ID родительского комментария (опционально)")
        };
    }
}
