package com.ub.backend.base.service;

import com.ub.backend.base.api.response.FileResponse;
import com.ub.core.base.exception.BadRequestApiException;
import com.ub.core.picture.model.PictureDoc;
import com.ub.core.picture.repository.PictureRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class PictureApiService {
    @Autowired private PictureRepository pictureRepository;

    public FileResponse uploadPicture(MultipartFile picture, String fileName) throws BadRequestApiException {
        PictureDoc pictureDoc = pictureRepository.save(picture);
        if(pictureDoc == null)
            throw new BadRequestApiException("base.uploadImage", "file_content", "error.invalid");

        if (fileName != null) {
            pictureDoc.setFileName(fileName);
            pictureRepository.save(pictureDoc);
        }

        return new FileResponse(pictureDoc.getId().toString());
    }

    public String removePicture(ObjectId pictureId) {
        pictureRepository.remove(pictureId);
        return "OK";
    }
}
