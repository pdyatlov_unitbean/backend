<%@ page import="com.ub.core.base.routes.BaseRoutes" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--@elvariable id="pageHeader" type="com.ub.core.base.view.PageHeader"--%>

<c:if test="${not empty pageHeader}">
    <div id="breadcrumbs-wrapper">
        <div class="container">
            <div class="row">
                <div class="col s12 m7 l6">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="<%= BaseRoutes.ADMIN %>">
                                <i class="mdi-action-dashboard"></i> <s:message code="ubcore.admin.dashboard"
                                                                                text="Dashboard"/>
                            </a>
                        </li>

                        <c:forEach items="${pageHeader.breadcrumbs.links}" var="breadcrumbsLinks" varStatus="stat">
                            <li>
                                <a <c:if test="${not empty breadcrumbsLinks.link}">
                                    href="<c:url value="${breadcrumbsLinks.link}"/>"
                                </c:if>>${breadcrumbsLinks.title}</a>
                            </li>
                        </c:forEach>

                        <li class="active">${pageHeader.breadcrumbs.currentTitle}</li>
                    </ol>
                </div>
                <div class="col s12 m5 l6">
                    <c:forEach items="${pageHeader.links}" var="link">
                        <a style="margin-left: 5px" href="${link.link}"
                           class="btn btn-small ${not empty link.type ? link.type : "green"} waves-effect waves-light right">
                                ${link.title}
                        </a>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
</c:if>