<%@ page import="com.ub.core.file.routes.FileAdminRoutes" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--@elvariable id="search" type="com.ub.core.file.view.all.SearchFileAdminResponse"--%>

<%--<div class="section">
    <form:form class="row" action="<%= FileAdminRoutes.ALL%>" modelAttribute="search" method="get">
        <div class="input-field col s9 m10 l11">
            <form:input id="search" type="text" path="query" placeholder="Search"/>
        </div>
        <div class="input-field col s3 m2 l1">
            <div class="input-field">
                <form:button class="btn-floating waves-effect waves-light right" type="submit">
                    <i class="mdi-action-search"></i>
                </form:button>
            </div>
        </div>
    </form:form>
</div>--%>

<div class="section">
    <table class="responsive-table bordered">
        <thead>
        <tr>
            <th>ID</th>
            <th>Private</th>
            <th>URL</th>
            <th><s:message code="ubcore.admin.createdAt"/></th>
            <th><s:message code="ubcore.admin.updateAt"/></th>

            <th><s:message code="ubcore.admin.action"/></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${search.result}" var="doc" varStatus="status">
            <tr>
                <td>${doc.id}</td>
                <td>${doc.getPrivate()}</td>
                <td>
                    <a href="/files/${doc.id}" target="_blank">/pics/${doc.id}</a>
                </td>
                <td><fmt:formatDate type="both" dateStyle="short" timeStyle="short"
                                    value="${doc.createdAt}"/></td>
                <td><fmt:formatDate type="both" dateStyle="short" timeStyle="short"
                                    value="${doc.updateAt}"/></td>

                <td>
                    <c:url value="<%= FileAdminRoutes.REMOVE %>" var="deleteUrl">
                        <c:param name="id" value="${doc.id}"/>
                    </c:url>
                    <a href="${deleteUrl}" class="btn btn-small waves-effect waves-light red js-link-remove-doc">
                        <s:message code="ubcore.admin.action.remove"/><i class="mdi-action-delete left"></i>
                    </a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

<div class="section">
    <div class="row center-align">
        <ul class="pagination">
            <c:url value="<%=FileAdminRoutes.ALL%>" var="prevUrl">
                <c:param name="query" value="${search.query}"/>
                <c:param name="currentPage" value="${search.prevNum}"/>
            </c:url>
            <li class="${search.prevNum eq search.currentPage ? 'disabled' : 'waves-effect'}">
                <a <c:if test="${search.prevNum ne search.currentPage}">href="${prevUrl}"</c:if>>
                    <i class="mdi-navigation-chevron-left"></i>
                </a>
            </li>

            <c:forEach items="${search.pagination}" var="page">
                <c:url value="<%=FileAdminRoutes.ALL%>" var="pageUrl">
                    <c:param name="query" value="${search.query}"/>
                    <c:param name="currentPage" value="${page}"/>
                </c:url>
                <li class="${search.currentPage eq page ? 'active' : ''}">
                    <a href="${search.currentPage ne page ? pageUrl : ''}">${page + 1}</a>
                </li>
            </c:forEach>

            <c:url value="<%=FileAdminRoutes.ALL%>" var="nextUrl">
                <c:param name="query" value="${search.query}"/>
                <c:param name="currentPage" value="${search.nextNum}"/>
            </c:url>
            <li class="${search.nextNum eq search.currentPage ? 'disabled' : 'waves-effect'}">
                <a <c:if test="${search.nextNum ne search.currentPage}">href="${nextUrl}"</c:if>>
                    <i class="mdi-navigation-chevron-right"></i>
                </a>
            </li>
        </ul>
    </div>
</div>