<section>
    <div class="container article-title">
        <div class="row">
            <h1 class="col-md-12 article-title-h1">
                Что нового в IOS 10
            </h1>
        </div>
    </div>
</section>

<section>
    <div class="container article-pic">
        <div class="row">
            <div class="col-md-12">
                <img class="article-pic-img" src="/static/backend/img/article-pic.jpg"/>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
         <div class="row">
             <div class="col-md-12 article-content">
                 Сегодня Крейг Федериги, вице-президент Apple, представил «самое большое» обновление (по его словам) iOS. Новая версия мобильной операционной системы Apple включает совершенно новый экран блокировки, обновленные уведомления, расширенную поддержку 3D Touch и новые инструменты для взаимодействия с приложениями. Заинтригованы? Давайте подробнее.

                 Вот как выглядит обновленный экран блокировки. Появилась возможность совершать действия с уведомлениями прямо на данном экране при помощи 3D Touch, кнопка «очистить все уведомления» и другие возможности.

                 Удивительные изменения претерпел поиск Spotlight. Здесь теперь не только предложения Siri и результаты поиска, но и другая полезная информация: результаты матчей, ваш календарь, погода и даже воспроизведение видео!
             </div>
         </div>
    </div>
</section>

<section>
    <div class="container article-comment-add">
        <div class="row">
             <div class="col-sm-1">
                 <div class="article-comment-add-userpic"></div>
             </div>
            <div class="col-sm-10">
                <div class="article-comment-add-comment">
                    <input class="article-comment-add-comment-input">
                </div>
            </div>
            <div class="col-sm-1">
                <div class="article-comment-add-submit">
                    <button class="article-comment-add-submit-btn"></button>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container article-comment-list">
        <div class="row">

            <div class="col-md-12 article-comment-list-item">
                <div class="article-comment-list-userpic"></div>
                <div class="article-comment-list-content">
                    <div class="article-comment-list-content-date">
                        13 июня 2016
                    </div>
                    <div class="article-comment-list-content-text">
                        А вообще на андроид похоже
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>